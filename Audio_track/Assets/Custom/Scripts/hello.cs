﻿using UnityEngine;

public class hello : MonoBehaviour
{
    public AudioClip Shout;
    public AudioClip letMeIn;

    private AudioSource source;
    private float vollowRange = .5f;
    private float volHighRange = 1.0f;
    // Start is called before the first frame update
    void Start()
    {
        source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            source.PlayOneShot(Shout, 1F);        
        }

        if (Input.GetButtonDown("Fire2"))
        {
            source.PlayOneShot(letMeIn, 1F);
        }


    }
}
