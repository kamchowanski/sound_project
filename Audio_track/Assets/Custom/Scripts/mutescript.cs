﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class mutescript : MonoBehaviour
{
    public AudioMixerSnapshot Mute;
    public AudioMixerSnapshot UnMute;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            mute();
        }

        if (Input.GetKeyDown(KeyCode.N))
        {
            unmute();
        }

    }

    public void mute()
    {
        Mute.TransitionTo(1f);   
    }

    public void unmute()
    {
        UnMute.TransitionTo(1f);
    }
}
