﻿using UnityEngine;

public class ThrowObject : MonoBehaviour
{
    public GameObject projectile;
    public AudioClip shootSound;

    private float throwSpeed = 200f;
    private AudioSource source;
    private float vollowRange = .5f;
    private float volHighRange = 1.0f;
    // Start is called before the first frame update
    void Start()
    {
        source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            source.PlayOneShot(shootSound, 1F);
            GameObject throwThis = Instantiate(projectile, transform.position, transform.rotation) as GameObject;
            throwThis.GetComponent<Rigidbody>().AddRelativeForce(new Vector3(0, 0, throwSpeed));
        }
    }
}
